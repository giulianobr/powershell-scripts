Import-Module ActiveDirectory
$SearchBase = "OU=YOUR_OU,DC=COMPANY,DC=com"
Get-ADUser -Filter 'Mail -like "*YOUR_CURRENT_DOMAIN.com"' -SearchBase $SearchBase -Properties "mail" | Foreach-Object {
   Set-ADUser -Identity $_ -Add @{ProxyAddresses=($_.mail)}
   Set-ADUser -Identity $_ -Email ($_.mail -replace "@YOUR_CURRENT_DOMAIN.com","@YOUR_NEW_DOMAIN.com")
}
